var selectedRow = null
function fnSave() {

    let StudentId = document.getElementById('sid').value;
    let StudentName = document.getElementById('sname').value;
    let StudentEmail = document.getElementById('semail').value;
    let StudentClass = document.getElementById('sclass').value;
    let StudentYear = document.getElementById('syear').value;
    let StudentCity = document.getElementById('scity').value;
    let StudentCountry = document.getElementById('scountry').value;

    var data = {
        id: StudentId,
        name: StudentName,
        email: StudentEmail,
        class: StudentClass,
        year: StudentYear,
        city: StudentCity,
        country: StudentCountry
    }

    var table = document.getElementById("tab").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.id;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.name;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.email;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.class;
    cell5 = newRow.insertCell(4);
    cell5.innerHTML = data.year;
    cell6 = newRow.insertCell(5);
    cell6.innerHTML = data.city;
    cell7 = newRow.insertCell(6);
    cell7.innerHTML = data.country;
    cell7 = newRow.insertCell(7);
    cell7.innerHTML = `<button onClick="onEdit(this)">Edit</button>&nbsp;&nbsp
                        <button onClick="onDelete(this)">Delete</button>`;
                        fnClear();

}





function fnClear() {

    let StudentId = document.getElementById('sid').value = "";
    let StudentName = document.getElementById('sname').value = "";
    let StudentEmail = document.getElementById('semail').value = "";
    let StudentClass = document.getElementById('sclass').value = "";
    let StudentYear = document.getElementById('syear').value = "";
    let StudentCity = document.getElementById('scity').value = "";
    let StudentCountry = document.getElementById('scountry').value = "";

}

function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById("sid").value = selectedRow.cells[0].innerHTML;
    document.getElementById("sname").value = selectedRow.cells[1].innerHTML;
    document.getElementById("semail").value = selectedRow.cells[2].innerHTML;
    document.getElementById("sclass").value = selectedRow.cells[3].innerHTML;
    document.getElementById("syear").value = selectedRow.cells[4].innerHTML;
    document.getElementById("scity").value = selectedRow.cells[5].innerHTML;
    document.getElementById("scountry").value = selectedRow.cells[6].innerHTML;
}
function onFormSubmit() {
    if (validate()) {
        var formData = readFormData();
        if (selectedRow == null)
            insertNewRecord(formData);
        else
            updateRecord(formData);
        resetForm();
    }
}
function readFormData() {
    var formData = {};
    formData["sid"] = document.getElementById("sid").value;
    formData["sname"] = document.getElementById("sname").value;
    formData["semail"] = document.getElementById("semail").value;
    formData["syear"] = document.getElementById("syear").value;
    formData["scity"] = document.getElementById("scity").value;
    formData["scountry"] = document.getElementById("scountry").value;
    return formData;
}
function validate() {
    isValid = true;
    if (document.getElementById("sid").value == "") {
        isValid = false;
        document.getElementById("fullNameValidationError").classList.remove("hide");
    } else {
        isValid = true;
        if (!document.getElementById("fullNameValidationError").classList.contains("hide"))
            document.getElementById("fullNameValidationError").classList.add("hide");
    }
    return isValid;
}
function updateRecord(formData){


    selectedRow.cells[0].innerHTML = formData.sid;
    
    selectedRow.cells[0].innerHTML = formData.sname;
    
    selectedRow.cells[0].innerHTML = formData.semail;
    
    selectedRow.cells[0].innerHTML = formData.syear;
    
    selectedRow.cells[0].innerHTML = formData.scity;
    
    selectedRow.cells[0].innerHTML = formData.scountry;
    
    }